# BF Parser

Simply brain-fcuk language parser implemented using python 

## Installation

Just Python 2.7 is required.

## Usage
 
```
$ python bf.py ./data/hello_world
```

## Development

It is simply django apps which introduce channels idea to make django a real time web framework.

## Copyright

Copyright (c) 2016 Pawel Buchowski.
