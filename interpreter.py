#!/usr/bin/python
import sys

def interprete(code):
    tape, index, cell_pos= [], 0, 0
    loopsmap = loops(code)

    while len(code) > index:
      command = code[index]

      try:
          test = tape[cell_pos]
      except:
          while(len(tape) == cell_pos):
              tape.append(0)

      if command == '+':
          tape[cell_pos] += 1 if tape[cell_pos] < 255 else 0
      elif command == '-':
          tape[cell_pos] -= 1 if tape[cell_pos] > 0 else 255
      elif command == '[':
            if tape[cell_pos] == 0:
                index = loopsmap[index]
      elif command == ']':
            if tape[cell_pos] != 0:
                index = loopsmap[index]
      elif command == '<':
          cell_pos -= 1
      elif command == '>':
          cell_pos += 1
      elif command == '.':
          sys.stdout.write(chr(tape[cell_pos]))
      elif command == ',':
          tape[cell_pos] = input()
      index +=1

def loops(code):
    tmp, loopsmap = [], {}
    for pos, cmd in enumerate(code):
        if cmd == "[": tmp.append(pos)
        if cmd == "]":
            start = tmp.pop()
            loopsmap[start] = pos
            loopsmap[pos] = start
    return loopsmap