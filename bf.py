#!/usr/bin/python

import sys
from interpreter import interprete

allowed_syntax = [',', '.', '[', ']', '<', '>', '+', '-']

class MySyntaxError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return "Invalid syntax in column: %s" % self.value


def open_data_file(filepath):
    f = open(filepath, "r")
    code = f.read()
    f.close()

    return code


def validate(code):
    for s in code:
        if s not in allowed_syntax:
            raise MySyntaxError(code.index(s))


def main():
    if len(sys.argv) == 2:
        code = open_data_file(sys.argv[1])
    else:
        raise IOError("Help: ./%s <path to data file>" % __file__)
    validate(code)
    interprete(code)

main()
